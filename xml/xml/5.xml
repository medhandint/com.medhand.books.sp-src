<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE component PUBLIC "-//JWS//DTD WileyML 20060316 Vers 2.1 WileyML//EN" "../DTD/medhand.dtd">
<html>
<body lang="EN-US" link="blue" vlink="purple">
<div class="Section1">
<p>There are a number of factors that increase the risk of violence by patients in the
accident and emergency department. Firstly, staff do not have specific training in
mental health and often feel less confident in managing patients with psychiatric
problems. Patients regularly present in crisis, including acute psychotic episodes
and heightened emotional states. Such episodes may be directly a result of, or
compounded by, alcohol and drugs, which are associated with an increased risk
of violence. In addition, it may be difficult to obtain background information to
inform a risk assessment. Generally, a psychiatrist assessing a patient in the accident
and emergency department should carefully consider what precautions need to
be taken.</p>
<p><b>Vignette – A drip stand an offensive weapon?</b></p>
<p>One evening, Margaret, who is 43 years old, presents to her local accident and emergency
department complaining of feeling unwell. She reports that she has a diagnosis of bipolar
affective disorder and feels that her illness may be relapsing. She has been prescribed lithium
and admits to having taken extra doses to try and control her mood. The triage nurse notes
that the patient appears agitated and somewhat confused. He takes a blood sample for
investigations, including a lithium level, and hangs up a saline drip. The patient is asked to
wait in a cubicle to see a doctor. The patient becomes increasingly impatient and angry. She
begins to pace around the department brandishing a drip stand and shouting that she wants
to see a doctor immediately. The staff contact the on-call psychiatry senior house officer for
assistance.</p>
<p>What points should the assessing psychiatrist consider?</p>
<ul>
<li>Risk assessment: Who is at risk? Those to be considered include the patient
(both a risk of inadvertent self-injury and the risks associated with an
untreated lithium toxicity), other patients, staff and the psychiatrist. </li>
<li>De-escalation: Depending on the risk assessment, the psychiatrist may try
basic de-escalation techniques, including a calm and reassuring approach,
offering to help the patient. </li>
<li>Control and restraint: If the patient does not respond to de-escalation, and
continues to pose a risk to herself or others, control and restraint may be
required. This should only be carried out by trained staff, to avoid
unnecessary injury to the patient and staff involved. This will usually
require hospital security staff or, where this is not possible, the police. </li>
<li>Interview room: A quiet environment may be important in helping to
settle the patient. The psychiatrist should consider what precautions are
necessary before assessing the patient. A chaperone (nursing or security
staff) may be required. The charge nurse in the accident and emergency
department should be made aware of the plan. </li>
<li>Background information: Ideally, there should be access to the mental
health electronic information system in the accident and emergency
department. This might include the patient’s care plan and a risk
management plan that will inform the assessment. </li>

</ul>
<p>The points below are safety issues in the accident and emergency department
that are relevant to an individual psychiatrist. They are adapted from Council
Report CR118 Psychiatric Services to Accident and Emergency Departments (Royal
College of Psychiatrists &amp; British Association for Accident and Energency Medicine,
2004), which also contains a broader discussion of safety in the accident and
emergency department.</p>

<p>The Council Report CR118 contains recommendations for interview facilities for
psychiatric consultations in the accident and emergency department. These include
the need for a properly equipped interview room. This room should be close to the
main receiving area. It should have two outward opening doors with observation
windows, and an alarm system alerting staff nearby or a suitable security system.</p>
<p>A closed-circuit television (CCTV) security system is an additional safeguard.
Although technology (such as CCTV) can be of assistance in improving safety, it
should only be employed in association with the implementation of good practice.</p>
<p>Where there is no specific psychiatric interview room, staff should take particular
care to consider what precautions may be necessary to safeguard themselves
when interviewing patients.</p>


<p><b>Background information</b></p>
<p>It is a sensible precaution for staff to have prior warning about patients known to
pose a risk of violence, in order to ensure that the patient is assessed in conditions
that keep the risk to staff at a minimum.</p>
<p>Where possible, background information should be obtained prior to interview.
Swift access to case records is therefore an important safety issue. Warnings
about propensity for violence should be written in a prominent place in the
mental health case notes and, where possible, be recorded on the computer
information system in the accident and emergency department.</p>
<p><b>Interviewing</b></p>
<p>Although patients have a right to privacy, confidentiality and respect for their
cultural sensitivities, this must be balanced against the staff’s right not to be
assaulted, and to take appropriate precautions to minimise this risk.</p>
<p>Staff interviewing a patient should always inform a senior member of the
accident and emergency nursing staff before commencing the interview. There should be agreed procedures regarding chaperones: the presence of a chaperone
should be regarded as the norm, and interviews without chaperones should only
proceed after discussion with relevant staff. It should be a conscious and a
consensus decision.</p>
<p>Departmental procedures should include guidance on regular checks via the
observation window while the interview is taking place (e.g. every 5 min).</p>
<p><b>Learning from incidents</b></p>
<p>A jointly agreed and understood protocol for the reporting of untoward incidents
should be in place. This will only work if the culture allows staff to feel comfortable
about reporting incidents without prejudice. Reporting incidents should be linked
to a structure that allows learning to take place, and adaptation of practices as a
result of incidents.</p>
</div></body></html>